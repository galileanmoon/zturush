using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBehaviour : MonoBehaviour
{
    [SerializeField]
    public float speed;
    public Transform player;
    private Rigidbody2D rb;
    private Vector3 direction;


    void Start()
    {
        rb = GetComponent<Rigidbody2D>();

    }

    void Update()
    {
        direction = player.position - transform.position;
    }
    private void FixedUpdate()
    {
        moveCharacter(direction);
    }
    void moveCharacter(Vector2 direction)
    {
        rb.MovePosition((Vector2)transform.position + (direction * speed * Time.fixedDeltaTime));
    }
}
